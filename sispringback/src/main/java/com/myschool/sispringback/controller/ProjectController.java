package com.myschool.sispringback.controller;

import com.myschool.sispringback.entity.Member;
import com.myschool.sispringback.entity.Project;
import com.myschool.sispringback.mapper.ProjectMapper;
import com.myschool.sispringback.repository.MemberRepository;
import com.myschool.sispringback.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projects")
public class ProjectController {

  private final ProjectRepository projectRepository;
  private final MemberRepository memberRepository;

  @Autowired
  public ProjectController(ProjectRepository projectRepository, MemberRepository memberRepository) {
    this.projectRepository = projectRepository;
    this.memberRepository = memberRepository;
  }

  @GetMapping("/members/{memberId}")
  public List<Project> getAllProjectsByMemberId(@PathVariable Long memberId) {
    Member member = memberRepository.findById(memberId)
            .orElseThrow(() -> new IllegalArgumentException("Member not found"));

    return member.getProjects();
  }


  @PostMapping("/members/{memberId}")
  public Project addProject(@RequestBody Project project, @PathVariable Long memberId) {
    Member member = memberRepository.findById(memberId).orElseThrow(() -> new IllegalArgumentException("Member not found"));
    project.setMember(member);
    return projectRepository.save(project);
  }


  @GetMapping("/{projectId}")
  public ResponseEntity<Project> getProjectById(@PathVariable Long projectId) {
    Optional<Project> optionalProject = projectRepository.findById(projectId);

    if (optionalProject.isPresent()) {
      Project project = optionalProject.get();
      return new ResponseEntity<>(project, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PatchMapping("/{projectId}")
  public ResponseEntity<Project> updateProject(@PathVariable(name = "projectId") Long projectId, @RequestBody Project projectToUpdate) {
    Optional<Project> optionalProject = projectRepository.findById(projectId);

    if (optionalProject.isPresent()) {
      Project projectToSaveInBdd = ProjectMapper.updateProjectMapper(projectId, projectToUpdate);
      projectRepository.save(projectToSaveInBdd);

      return new ResponseEntity<>(projectToSaveInBdd, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{projectId}")
  public ResponseEntity<String> deleteProject(@PathVariable(name = "projectId") Long projectId) {
    Optional<Project> optionalProject = projectRepository.findById(projectId);

    if (optionalProject.isPresent()) {
      projectRepository.deleteById(projectId);
      return new ResponseEntity<>("Project with ID " + projectId + " deleted successfully", HttpStatus.NO_CONTENT);
    } else {
      return new ResponseEntity<>("Project with ID " + projectId + " not found", HttpStatus.NOT_FOUND);
    }
  }
}
