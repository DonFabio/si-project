'use client';

import eventToUpdateLayout from "@/app/events/eventToUpdateLayout";
import Image from "next/image";
import { useEffect, useState } from 'react';
import { FaUser } from "react-icons/fa";
import Link from 'next/link'
import { FaHome } from "react-icons/fa";

export default function Header() {
  const [connectionState, setconnectionState] = useState<any>('');

  const handleDisconnectClick = () => {
    localStorage.removeItem('currentUser');
    setconnectionState(getConnectionState(null));    
  }

  const getConnectionState = (currentUser: any) => {
    if (currentUser === null) {
      return (
        <>
          <FaUser color="#979ea6" size={25} />
          <p className="text-sm text-slate-500">Non connecté</p>
        </>
      );
    } else {
      return (
        <>
          <FaUser color="#3b82f6" size={25} />
          <Link 
            href="/"
            onClick={handleDisconnectClick}
            className="text-sm text-blue-500 cursor-pointer hover:underline">
            se déconnecter
          </Link>
        </>
      )
    }
  }

  const handleConnectionStateFromOutside = () => {
    const currentUser = localStorage.getItem('currentUser');
    setconnectionState(getConnectionState(currentUser));
  }

  useEffect(() => {
    const currentUser = localStorage.getItem('currentUser');
    setconnectionState(getConnectionState(currentUser));

    eventToUpdateLayout.on('updateLayout', handleConnectionStateFromOutside);

    return () => {
      eventToUpdateLayout.off('updateLayout', handleConnectionStateFromOutside);
    };

  }, []);
  
  return (
    <header className="px-8 py-2 border-b-2 border-gray-400 flex justify-between items-center">
      <Link 
        href="/"
        onClick={handleDisconnectClick}
      >
        <FaHome color="#979ea6" size={30} className="cursor-pointer"
          onClick={handleConnectionStateFromOutside}
        />
      </Link>
      <Image src="/logo.png" alt="logo" width={60} height={60} />
      <div className="flex justify-end items-center">
        <div className="flex flex-col items-end">
          {connectionState}
        </div>
      </div>
    </header>
  );
}