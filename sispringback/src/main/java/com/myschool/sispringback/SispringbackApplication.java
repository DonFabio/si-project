package com.myschool.sispringback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SispringbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(SispringbackApplication.class, args);
	}

}
