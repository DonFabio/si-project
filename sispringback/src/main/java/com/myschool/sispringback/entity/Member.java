package com.myschool.sispringback.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Member {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  private String username;
  private String pass;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  @JsonIgnore
  @OneToMany(mappedBy = "member")
  List<Project> projects = new ArrayList<>();

  public List<Project> getProjects(){
    return this.projects;
  }
  public void addProject(Project project) {
    this.projects.add(project);
  }
  public void removeProject(Project project) {
    this.projects.remove(project);
  }
}
