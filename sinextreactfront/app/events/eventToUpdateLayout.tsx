import { EventEmitter } from 'events';

const eventToUpdateLayout = new EventEmitter();

export default eventToUpdateLayout;