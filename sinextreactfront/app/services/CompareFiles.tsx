async function extractOccurrencesFromFile(file: File, regex: RegExp): Promise<Array<string>> {
  const reader: FileReader = new FileReader();

  const fileContent = await new Promise<string>((resolve, reject) => {
    reader.onload = (event: any) => {
      resolve(event.target.result);
    };
    reader.onerror = (error) => {
      reject(error);
    };
    reader.readAsText(file);
  });
  const occurrences: Array<string> = [];
  let match: RegExpExecArray | null;

  while ((match = regex.exec(fileContent)) !== null) {
    occurrences.push(match[1]);
  }
  return occurrences;
}

export interface ComparisonResult {
  bpmnFlux: string;
  mcfFlux: string;
  result: boolean
}

function createComparisonResultObjectFromComparison(array1: Array<string>, array2: Array<string>): Array<ComparisonResult> {
  let result:Array<ComparisonResult> = [];

  array1.forEach((element: string) => {
    const matchingIndex: number = array2.indexOf(element);
    result.push({
      bpmnFlux: element,
      mcfFlux: matchingIndex !== -1 ? element : 'missing',
      result: matchingIndex !== -1
    });
  });

  array2.forEach((element: string) => {
    if (array1.indexOf(element) === -1) {
      result.push({
        bpmnFlux: 'missing',
        mcfFlux: element,
        result: false
      });
    }
  });
  return result;
}

export function calculatePercentage(resultArray: Array<ComparisonResult>): string {
  const falseCount: number = resultArray.filter(entry => entry.result === false).length;
  const total: number = resultArray.length;

  const falsePercentage: number = (falseCount / total) * 100;

  return falsePercentage.toFixed(2) + '%';
}

export async function getResultCompareFiles(bpmnFile: File, mfcFile: File): Promise<Array<ComparisonResult>> {

  const bpmnFluxRegex = /<bpmn:sequenceFlow[^>]*\sname="([^"]*)"/g;
  const mfcFluxRegex = /<mxCell[^>]*\svalue="([^"]*)"\sstyle="[^"]*edgeLabel[^"]*"/g;

  const bpmnFlux: Array<string> = await extractOccurrencesFromFile(bpmnFile, bpmnFluxRegex);
  const mfcFlux: Array<string> = await extractOccurrencesFromFile(mfcFile, mfcFluxRegex);

  const result: Array<ComparisonResult> = createComparisonResultObjectFromComparison(bpmnFlux, mfcFlux);

  return result;
}