import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ToastContainer } from 'react-toastify';
import "./globals.css";
import "react-toastify/dist/ReactToastify.css";

import Header from "./components/UI/header";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "SI Project",
  description: "Compare BPMN flux and MFC flux",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Header />
        {children}
        <ToastContainer />
      </body>
    </html>
  );
}
