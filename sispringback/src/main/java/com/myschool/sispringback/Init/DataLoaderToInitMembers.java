package com.myschool.sispringback.Init;

import com.myschool.sispringback.entity.Member;
import com.myschool.sispringback.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoaderToInitMembers implements CommandLineRunner {

  @Autowired
  private MemberRepository memberRepository;

  @Override
  public void run(String... args) {
    Member member1 = new Member();
    Member member2 = new Member();
    Member member3 = new Member();

    member1.setUsername("u1");
    member1.setPass("p1");
    member2.setUsername("u2");
    member2.setPass("p2");
    member3.setUsername("u3");
    member3.setPass("p3");

    memberRepository.save(member1);
    memberRepository.save(member2);
    memberRepository.save(member3);
  }
}
