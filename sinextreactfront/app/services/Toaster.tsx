import { toast } from 'react-toastify';
import { ToastOptions } from '../../node_modules/react-toastify/dist/types';

export enum ToasterType {
  ERROR,
  SUCCESS
}

const myToaster: ToastOptions<unknown> = {
  position: "top-right",
  autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: true,
  pauseOnHover: true,
  draggable: true,
  progress: undefined,
  theme: "colored",
}

export function toaster(message: string, toasterType: ToasterType) {
  if (toasterType === ToasterType.ERROR) {
    toast.error(message, myToaster);
  }
  if (toasterType === ToasterType.SUCCESS) {
    toast.success(message, myToaster);
  }
}