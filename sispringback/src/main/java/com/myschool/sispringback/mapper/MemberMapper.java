package com.myschool.sispringback.mapper;

import com.myschool.sispringback.entity.Member;

import java.util.function.Consumer;

public class MemberMapper {
  public static Member updateMemberMapper(Long memberId, Member memberToUpdate) {
    Member memberToSaveInBdd = new Member();
    memberToSaveInBdd.setId(memberId);
    updateIfNotNull(memberToSaveInBdd::setUsername, memberToUpdate.getUsername());
    return memberToSaveInBdd;
  }
  private static <T> void updateIfNotNull(Consumer<T> setter, T value) {
    if (value != null) {
      setter.accept(value);
    }
  }
}
