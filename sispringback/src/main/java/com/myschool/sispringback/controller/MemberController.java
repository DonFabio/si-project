package com.myschool.sispringback.controller;

import com.myschool.sispringback.entity.Member;
import com.myschool.sispringback.mapper.MemberMapper;
import com.myschool.sispringback.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/members")
public class MemberController {

  private final MemberRepository memberRepository;

  @Autowired
  public MemberController(MemberRepository memberRepository) {
    this.memberRepository = memberRepository;
  }

  @GetMapping
  public List<Member> getAllMembers() {
    return memberRepository.findAll();
  }


  @PostMapping(("/login"))
  public ResponseEntity<Member> login(@RequestBody Member member) {
    Member existingMember = memberRepository.findByUsername(member.getUsername());

    if (existingMember != null && existingMember.getPass().equals(member.getPass())) {
      return ResponseEntity.ok(existingMember);
    } else {
      return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(null);
    }
  }

  @PostMapping
  public Member addMember(@RequestBody Member member) {
    return memberRepository.save(member);
  }


  @GetMapping("/{MemberId}")
  public ResponseEntity<Member> getMemberById(@PathVariable Long memberId) {
    Optional<Member> optionalMember = memberRepository.findById(memberId);

    if (optionalMember.isPresent()) {
      Member member = optionalMember.get();
      return new ResponseEntity<>(member, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @PatchMapping("/{memberId}")
  public ResponseEntity<Member> updateMember(@PathVariable(name = "memberId") Long memberId, @RequestBody Member memeberToUpdate) {
    Optional<Member> optionalMember = memberRepository.findById(memberId);

    if (optionalMember.isPresent()) {
      Member memberToSaveInBdd = MemberMapper.updateMemberMapper(memberId, memeberToUpdate);
      memberRepository.save(memberToSaveInBdd);

      return new ResponseEntity<>(memberToSaveInBdd, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping("/{memberId}")
  public ResponseEntity<String> deleteMember(@PathVariable(name = "memberId") Long memberId) {
    Optional<Member> optionalMember = memberRepository.findById(memberId);

    if (optionalMember.isPresent()) {
      memberRepository.deleteById(memberId);
      return new ResponseEntity<>("Member with ID " + memberId + " deleted successfully", HttpStatus.NO_CONTENT);
    } else {
      return new ResponseEntity<>("Member with ID " + memberId + " not found", HttpStatus.NOT_FOUND);
    }
  }
}
