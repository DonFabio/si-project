'use client'

import { MdDelete } from "react-icons/md";
import { useEffect, useState } from 'react';
import { getProjects, Project, deleteProject, createProjectByMemberId } from "../../../../services/ProjectsApi";
import { useRouter } from 'next/navigation'
import { ToasterType, toaster } from "@/app/services/Toaster";
 
export default function Projects({ params }: { params: { id: string } }) {
  const router = useRouter()
  const memberId: number = parseInt(params.id);

  const [projectName, setProjectName] = useState('');

  const [projects, setProjects] = useState<Project[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [error, setError] = useState<Error | null>(null);
  const [projectNameIsEmpty, setProjectNameIsEmpty] = useState<boolean>(false);

  const handleDeleteProjectClick = async (id: number, e: React.MouseEvent<HTMLTableCellElement, MouseEvent>) => {
    e.stopPropagation();
    await deleteProject(id);
    await callGetProjects();
    toaster("Le projet a été supprimé avec succès", ToasterType.SUCCESS);
  };
  const handleSelectProjectClick = async (project: Project) => {
    localStorage.setItem('currentProject', project.name);
    router.push(`/project/${project.id}`, { scroll: false })
  };

  const handleCreateProjectClick = async () => {
    if (projectName === '') {
      setProjectNameIsEmpty(true)
    } else {
      await createProjectByMemberId(projectName, memberId);
      await callGetProjects();
      setProjectNameIsEmpty(false)
      setProjectName('');
    }
  };

  const callGetProjects = async () => {
    try {
      const responseData = await getProjects(memberId);
      setProjects(responseData);
      setIsLoading(false);
    } catch (error: any) {
      setError(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    
    callGetProjects();
  }, []);

  const messageIfInputToCreateMessageIsEmpty = projectNameIsEmpty === true ? 
    <div className="text-red-500 ml-8">Impossible de créer un projet avec un nom vide.</div> : '';
  
  const handleKeyPress = (e : any) => {
    if (e.key === 'Enter') {
      handleCreateProjectClick();
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (error) {
    return <div>Error: {error.message}</div>;
  }

  return (
    <main>
      <h1 className="m-8 text-2xl font-bold">Projects</h1>
      {messageIfInputToCreateMessageIsEmpty}
      <div className="flex items-center">
        <input
          className="mx-8 border-2 border-grey-600 rounded-md p-2 text-sm" type="text" name="projectName" placeholder="Nom du projet" 
          value={projectName}
          onChange={e => setProjectName(e.target.value)}
          onKeyDown={handleKeyPress}
        />
        <button
          onClick={handleCreateProjectClick}
          className="bg-blue-500 hover:bg-blue-600 text-white font-semibold px-4 h-8 rounded">
            créer un projet
        </button>
      </div>

      <div className=" m-8">
        <table className="w-full border-t-2 border-gray-300 rounded-lg">
          <tbody>

          {projects.map(project => {
            return (
              <tr
                key={project.id}
                className="p-2 border-b-2 border-gray-200 flex justify-between items-center hover:bg-blue-100 cursor-pointer"
                onClick={() => handleSelectProjectClick(project)}
              >
                <td>{project.name}</td>
                <td 
                  className="cursor-pointer"
                  onClick={(event) => handleDeleteProjectClick(project.id, event)}
                >
                  <MdDelete color="#979ea6" size={25} />
                </td>
              </tr>
            )
          })}
          </tbody>
        </table>
      </div>
    </main>
  );
}