'use client'

import { useState } from 'react';
import { login, Member } from './services/MembersApi';
import { useRouter } from 'next/navigation';
import { toaster, ToasterType } from './services/Toaster';
import eventToUpdateLayout from './events/eventToUpdateLayout';


export default function Home() {

  const router = useRouter();
  
  const [username, setUsername] = useState<string>('');
  const [pass, setPass] = useState<string>('');

  const handleClick = async () => {

    if (username === '' || pass === '') {
      toaster("miss username or password",ToasterType.ERROR);
    } else {
      try {
        const member: Member = await login(username, pass);
        toaster(`Welcome ${member.username} !`,ToasterType.SUCCESS);

        const jsonMember = JSON.stringify(member);

        localStorage.setItem('currentUser', jsonMember);
        eventToUpdateLayout.emit('updateLayout');
        
        router.push(`/projects/members/${member.id}`);

      } catch (error: any) {
        toaster(error.message,ToasterType.ERROR);
      }
    }
  }
  return (
    <main>
      <section className="flex justify-center mt-8">
        <div className="w-96 p-8 bg-gray-100 rounded-lg shadow-md">
          <h2 className="text-2xl font-semibold mb-4">Se connecter</h2>
          <form>
            <div className="mb-4">
              <label htmlFor="username" className="block text-gray-700 font-semibold mb-1">Nom d&apos;utilisateur</label>
              <input
                type="username"
                id="username"
                name="username"
                placeholder="Entrez votre nom d'utilisateur"
                className="w-full px-3 py-2 border rounded-lg focus:outline-none focus:border-blue-500"
                onChange={e => setUsername(e.target.value)}
              />
            </div>
            <div className="mb-4">
              <label htmlFor="password" className="block text-gray-700 font-semibold mb-1">Mot de passe</label>
              <input
                type="password"
                id="password"
                name="password"
                placeholder="Entrez votre mot de passe"
                className="w-full px-3 py-2 border rounded-lg focus:outline-none focus:border-blue-500"
                onChange={e => setPass(e.target.value)}
              />
            </div>
              <button 
                type="button"
                className="w-full bg-blue-500 text-white font-semibold py-2 rounded-lg hover:bg-blue-600 transition duration-200"
                onClick={handleClick}
              >
                Se connecter
              </button>
          </form>
        </div>
      </section>
    </main>
  );
}
