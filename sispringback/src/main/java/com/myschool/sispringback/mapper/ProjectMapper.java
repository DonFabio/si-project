package com.myschool.sispringback.mapper;

import com.myschool.sispringback.entity.Project;

import java.util.function.Consumer;

public class ProjectMapper {
  public static Project updateProjectMapper(Long projectId, Project projectToUpdate) {
    Project projectToSaveInBdd = new Project();
    projectToSaveInBdd.setId(projectId);
    updateIfNotNull(projectToSaveInBdd::setName, projectToUpdate.getName());
    return projectToSaveInBdd;
  }
  private static <T> void updateIfNotNull(Consumer<T> setter, T value) {
    if (value != null) {
      setter.accept(value);
    }
  }
}
