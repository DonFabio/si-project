export interface Project {
  id: number;
  name: string;
}

export async function createProjectByMemberId(projectName: string, memberId: number): Promise<Project> {
  try {
    const response = await fetch(`http://localhost:3002/projects/members/${memberId}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({name: projectName})
    });

    if (!response.ok) {
      throw new Error('Failed to post data');
    }

    const responseData = await response.json();

    return responseData;
  } catch (error) {
    throw error;
  }
}

export async function getProjects(memberId: number): Promise<Project[]> {
  try {
    const response = await fetch(`http://localhost:3002/projects/members/${memberId}`);
    if (!response.ok) {
      throw new Error('Failed to fetch data');
    }
    const responseData = await response.json();
    if (!Array.isArray(responseData)) {
      throw new Error('Invalid data format');
    }
    return responseData;
  } catch (error) {
    throw error;
  }
}

export async function deleteProject(id: number): Promise<boolean> {
  try {
    const response = await fetch(`http://localhost:3002/projects/${id}`, {
      
      method: 'DELETE',
    });
    if (!response.ok) {
      throw new Error('Failed to delete project');
    } else {
      return true;
    }
  } catch (error) {
    throw error;
  }
}
