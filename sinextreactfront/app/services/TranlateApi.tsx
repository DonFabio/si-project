import { ComparisonResult } from "./CompareFiles";
import { translate } from "google-translate-api-browser";

// to use the translation
// we have to go here https://cors-anywhere.herokuapp.com/corsdemo
// the click to the button to get this : "You currently have temporary access to the demo server."
// then you can use this module

const MISS = 'missing';

export async function translateText(results: ComparisonResult[], lang: string): Promise<ComparisonResult[]> {
  const translatedResults: ComparisonResult[] = [];

  for (let i = 0; i < results.length; i++) {
    const { bpmnFlux, mcfFlux } = results[i];

    // Réserver les 3 premiers caractères
    const bpmnPrefix = bpmnFlux === MISS ? '' : bpmnFlux.substring(0, 3);
    const mcfPrefix = mcfFlux === MISS ? '' : mcfFlux.substring(0, 3);

    const bpmnRest = bpmnFlux === MISS ? MISS : bpmnFlux.substring(3);
    const mfcRest = mcfFlux === MISS ? MISS : mcfFlux.substring(3);

    // Traduire le reste des chaînes
    const translatedBpmnFlux = bpmnRest === MISS ? MISS : await translateString(bpmnRest, lang);
    const translatedMcfFlux = mfcRest === MISS ? MISS : await translateString(mfcRest, lang);

    // Ajouter les résultats traduits au tableau
    translatedResults.push({
      bpmnFlux: bpmnPrefix + (bpmnPrefix.length > 0 ? " " : "") + translatedBpmnFlux,
      mcfFlux: mcfPrefix + (mcfPrefix.length > 0 ? " " : "") + translatedMcfFlux,
      result: results[i].result
    });
  }
  return translatedResults;
}

async function translateString(text: string, lang: string): Promise<string> {
  try {
    const translationResponse = await translate(text, { to: lang, corsUrl: "http://cors-anywhere.herokuapp.com/" });
    return translationResponse.text;
  } catch (error) {
    console.error("Erreur lors de la traduction :", error);
    return "error translation"; // En cas d'erreur, retourner une chaîne vide
  }
}