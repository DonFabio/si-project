'use client'

import { calculatePercentage, getResultCompareFiles, ComparisonResult } from "@/app/services/CompareFiles";
import { toaster, ToasterType } from "@/app/services/Toaster";
import { translateText } from "@/app/services/TranlateApi";
import { useState } from "react";

export default function Project() {

  let bpmnFile: File;
  let mfcFile: File;

  const [results, setResults] = useState<ComparisonResult[]>([]);
  const [percent, setPercent] = useState<string>('');
  const [lang, setLang] = useState<string>('fr');

  const getBpmnFile = (event: any) => {
    bpmnFile = event.target.files[0];
  };
  const getMfcFile = (event: any) => {
    mfcFile = event.target.files[0];
  };

  const handleCompareFilesClick = async () => {
    if (bpmnFile === undefined || mfcFile === undefined) {
      toaster("fichier importé manquant",ToasterType.ERROR);
      return;
    }      
      const results = await getResultCompareFiles(bpmnFile, mfcFile);
      setResults(results)

      const percent: string = calculatePercentage(results);
      setPercent(percent)

      toaster("Le calcul de comparaison s'est déroulé avec succès.",ToasterType.SUCCESS);
  };

  const langState = lang === 'fr' ? "Translate in english." : "Traduire en français.";
  let currentProjectName: any = '';

  if (typeof localStorage !== "undefined" && localStorage.getItem('currentProject')) {
    currentProjectName = localStorage.getItem('currentProject');
  }

  const handleToggleLang = async () => {
    const newLang = lang === 'fr' ? 'en' : 'fr';
    setLang(newLang);
    const newResult: ComparisonResult[] = await translateText(results, newLang);
    setResults(newResult);
  }

  const htmlResults = (
    <div>
      <h1 className="mt-4 text-2xl font-bold">Résultats</h1>
      <div>Le taux d&apos;incoherence : <span className="font-bold text-xl">{percent}</span></div>
      <div
        className="text-blue-600 underline cursor-pointer"
        onClick={handleToggleLang}
      >{langState}
      </div>
      <table className="mt-4 w-full border-2 border-gray-300 text-center">
        <thead>
          <tr>
            <th className="p-2 border-r-2 border-gray-200">Flux BPMN</th>
            <th className="p-2">Flux MFC</th>
          </tr>
        </thead>
        <tbody>
          {results.map((result, index) => {
            const color = result.result ? 'bg-green-100' : 'bg-red-100'
            return (
              <tr className={color} key={index}>
                <td className="border-2 border-gray-400" dangerouslySetInnerHTML={{ __html: result.bpmnFlux }} />
                <td className="border-2 border-gray-400" dangerouslySetInnerHTML={{ __html: result.mcfFlux }} />
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  );

  return (
    <main>
      <h1 className="m-8 text-2xl font-bold">Project {currentProjectName}</h1>

      <div className="m-8">
        <div className="mb-2">Sélectionner un fichier XML qui vient de l&apos;application BPMN.io représentant le BPMN :</div>
        <input className="mb-8" type="file" onChange={getBpmnFile} />
        <div className="mb-2">Sélectionner un fichier XML qui vient de l&apos;application Draw.io représentant le MFC :</div>
        <input type="file" onChange={getMfcFile} />

        <button
          className="mt-8 block bg-blue-500 hover:bg-blue-600 text-white font-semibold px-4 h-8 rounded"
          onClick={handleCompareFilesClick}
        >
            Comparer les fichiers
        </button>
        {percent === '' ? null : htmlResults}
      </div>
    </main>
  ); 
}
