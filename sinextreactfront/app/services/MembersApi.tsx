
export interface Member {
  id: Number,
  username: string;
  pass: string;
}

export async function login(username: string, pass: string): Promise<Member> {
  try {
    const response = await fetch(`http://localhost:3002/members/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({username, pass})
    });

    if (!response.ok) {
      if (response.status === 401) {
        throw new Error('Identifiants invalides');
      } else if (response.status === 403) {
        throw new Error('Accès refusé');
      } else {
        throw new Error('Erreur du serveur');
      }
    }

    const responseData = await response.json();

    return responseData;
  } catch (error) {
    if (error instanceof TypeError) {
      throw new Error('Erreur réseau : impossible de se connecter au serveur');
    } else {
      throw error;
    }
  }
}